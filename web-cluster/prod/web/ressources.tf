resource "aws_launch_configuration" "web_lc" {
  key_name        = aws_key_pair.deployer.key_name
  name            = "${var.env}-web_config_00"
  image_id        = var.conf["ami"]
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.allow_http.id]
  user_data       = <<-EOF
              #!/bin/bash
              apt update
              apt install apache2 php php-mysql php-curl python3 python3-pip -y                            
              cd /root/.ssh
              echo """-----BEGIN OPENSSH PRIVATE KEY-----
              b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
              NhAAAAAwEAAQAAAYEA8SLgjdACKYRddEhKul36p6lyOKjs/75u0zbFX+w7zr2BWSGa0CXQ
              Ov7cp01lbxDw/Jfg9ZHO5nGuzqsHe99fIM8nYurr2X2FdW6b+SSBLHtqszS+/C7Crl44UZ
              odvVauYB698H/tHx3nEIeX97JULKlXZa6J3jyBWHarAdGz9tN7LZHh33vCekC065v2ELrs
              B/6CtsFglJM+eZezUGpzIdATWJyLX5RI23aXeTpU358PAj63M2L+wyd5NnH1WglwCC8ZeO
              lR3u9Prp0IOLRrlO39xSHlgzvUSUWRXSzjUEqe6P+b972xoCSUl1I+YqoMokZtrMLc8zdp
              Vbm7d4G4x2dSsLgwl+9/ZBJT9THY8vudMvyWVZBMM2rzOCMXVmo7VDYkMdHbrjrcPqfaWs
              48qeA6aw2JowWfrdMTM4NUcEMnQ7xwsG0NyKYoTllIuqqZV9MY24MtGR2ZUusqjdfS9ItE
              /o/zVazghSDJZ6/11vZQZMkPPTarAORd8zxxxlT1AAAFiGqrvEFqq7xBAAAAB3NzaC1yc2
              EAAAGBAPEi4I3QAimEXXRISrpd+qepcjio7P++btM2xV/sO869gVkhmtAl0Dr+3KdNZW8Q
              8PyX4PWRzuZxrs6rB3vfXyDPJ2Lq69l9hXVum/kkgSx7arM0vvwuwq5eOFGaHb1WrmAevf
              B/7R8d5xCHl/eyVCypV2Wuid48gVh2qwHRs/bTey2R4d97wnpAtOub9hC67Af+grbBYJST
              PnmXs1BqcyHQE1ici1+USNt2l3k6VN+fDwI+tzNi/sMneTZx9VoJcAgvGXjpUd7vT66dCD
              i0a5Tt/cUh5YM71ElFkV0s41BKnuj/m/e9saAklJdSPmKqDKJGbazC3PM3aVW5u3eBuMdn
              UrC4MJfvf2QSU/Ux2PL7nTL8llWQTDNq8zgjF1ZqO1Q2JDHR26463D6n2lrOPKngOmsNia
              MFn63TEzODVHBDJ0O8cLBtDcimKE5ZSLqqmVfTGNuDLRkdmVLrKo3X0vSLRP6P81Ws4IUg
              yWev9db2UGTJDz02qwDkXfM8ccZU9QAAAAMBAAEAAAGBAJ+3CiTj8YquMXgjqdtnA+6pPQ
              5qVQ/PgnhDftfaEIMO4e23ZGMZqKJqzJvDl7vI7NVhg48j1HauNaqNfZd+i/OfQecy0qRr
              j0oIySZsF2LvG+02sok2L5xuq77g5XHTSAxCUGxLTzOdCr7YEL/vTfGG+GxQ/uBEEygU8Y
              9y5lBGIwA77++meVmgIjUsR/8sz8k7ujhyPRYrsOW4pPxOhZOPzkCS732PKfKyygFxsWWs
              ZLSH9bADkdqQ+qglMu0/8bs2unEOkaTYr0QYmn2eKHMuRpT8OC0qoQ7Qq3CECcJax0OEf0
              X0T2uKqFJaDG3GsP6Og6E+/k821QGQNNFf30YOiCewT+cu6rd+KDseSaonflYrpvPlJF1m
              Xw16ynYK6vTWuupY2TnBmIKr8W4mrw16SglWYVrEriRw6bQVw5p1dxF9slLGw0NHvPL7TH
              ur2fIT1jYpmPF7B70UfutVP++xYMWP1iF7EilvdWTlSWtSIa2gvZOh0S/bCfQK9rCSwQAA
              AMEA+TeaPXirmehRtKCWEnKW+OIPcWVMiSjVZDNiK8Xb9HSqlcPGNdP8dqMo5Wtab0g5UD
              ZCdG93VIQqh9r5eFfoUmUqEZNZL1OZ9VT+qIQ0d6pLQ9vqxQxRFTO2QFKC0A8yfSMryBWG
              CFbbkFOyeDLuhPauTUaKNd8211kNF7LnJVIRouXx1pbGYYRQu8o2mK/mvkfs8m3JUyUvAu
              qUJtL42iOa+UnvEz2VF1VimhTcxoLOs5L3LC1OX+1aDMUAZ+CuAAAAwQD/iWiHkQr6gyxp
              fB45x6OzlIa2gYZYUGYpFmDROLJmem4pUT5tim5sDoL2KikF2MfrKc97ojYABK+r7Wdz6J
              RTze8Bp/qe+OnRNG8xRid1bWuVQZUG1mgI668oFoMuM3iu7lNgFgH3wVO9ghWhyuQDgZAf
              PJnv9/WsG6gKbBok21Y8LMyy0xR5t7mZMhNQ9f3S1jCwJiMZTNpXs0GH8Gb+sdtco+Q4B1
              Cir9Svko5+OOzb6NylOr9zicnCJ7GO+dMAAADBAPGSySXBkTprE1eTqiviRV8vy2Vr7I8F
              Ayja9aikfX8QlPg6YUCsG0ozlLbAS0pmFQhDw7+w1myD8dpEyfSRNlhXiUwHfCHsMYOHLY
              6RjQR77qJLlRQmvAw9C7Tu5xhUYjXs2og0bAzwgig70xmvSZ881wBrb40Vuf6DiAFePF6B
              2du/7j/iH7+IbWrv6qNnq6rW7NQUw6Kofnw7PIh5bN/zK5jD+zKumWNsJoQPKkq00dfvgy
              bZczuQxz8EdSGxFwAAAA15YXNzaXJAdWJ1bnR1AQIDBA==
              -----END OPENSSH PRIVATE KEY-----
              """ > id_rsa
              chmod 400 id_rsa
              echo """
              Host *
                  StrictHostKeyChecking no
                  UserKnownHostsFile=/dev/null"""> config
              EOF
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "allow_http" {
  name        = "${var.env}-allow_http"
  description = "Allow TLS inbound traffic"

  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "http from VPC"
    from_port   = var.server_port
    to_port     = var.server_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http"
  }
}

resource "aws_autoscaling_group" "bar" {
  name                 = "${var.env}-foobar3-terraform-test"
  max_size             = 10
  min_size             = 2
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.web_lc.name
  vpc_zone_identifier  = data.aws_subnet_ids.default_subnets_ids.ids
  tag {
    key                 = "PROJECT"
    value               = "WEBAPP"
    propagate_at_launch = true
  }

  target_group_arns = [aws_lb_target_group.test.arn]
  health_check_type = "ELB"
}

resource "aws_lb_target_group" "test" {
  name     = "tf-example-lb-tg"
  port     = var.server_port
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id
  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 5
    timeout             = 2
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

}

resource "aws_lb" "example_alb" {
  name               = "${var.env}-terraform-asg-example"
  load_balancer_type = "application"
  subnets            = data.aws_subnet_ids.default_subnets_ids.ids
  security_groups    = [aws_security_group.alb.id]
}

resource "aws_security_group" "alb" {

  name = "${var.env}-alb-sg-http"

  # Allow inbound HTTP requests
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound requests
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.example_alb.arn
  port              = var.server_port
  protocol          = "HTTP"

  # By default, return a simple 404 page
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = 404
    }
  }
}

resource "aws_lb_listener_rule" "asg_rule" {
  listener_arn = aws_lb_listener.http.arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.test.arn
  }
}


