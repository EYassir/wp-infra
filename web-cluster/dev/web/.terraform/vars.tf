data "aws_vpc" "default"{
    default=true
}

data "aws_subnet_ids" "default_subnets_ids" {
  vpc_id = data.aws_vpc.default.id
}

variable "server_port"{
    default=80
    description ="web app port"
}

variable "conf"{
  default={
    "ami":"ami-0aef57767f5404a3c",
    "region":"eu-west-1"
  }
}
