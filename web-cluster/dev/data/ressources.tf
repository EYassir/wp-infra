resource "aws_db_instance" "default" {
  allocated_storage       = 20
  storage_type            = "gp2"
  engine                  = "mysql"
  engine_version          = "8.0"
  instance_class          = "db.t2.micro"
  name                    = "wordpress"
  username                = "admin"
  password                = "Pwdadmin"
  parameter_group_name    = "default.mysql8.0"
  backup_retention_period = 7
}
